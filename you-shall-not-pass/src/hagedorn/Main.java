package hagedorn;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {

    public static Pattern lowerRegex = Pattern.compile("^(?=.*[a-z])+");
    public static Pattern upperRegex = Pattern.compile("^?=.*([A-Z])+");
    public static Pattern numberRegex = Pattern.compile("^?=.*([1-9])");
    public static Pattern specialRegex = Pattern.compile("^?=.*([!#$%&'()*+,-./:;<=>?@\\[\\]^_`{|}~])");
    public static boolean lowerCheck = false;
    public static boolean upperCheck = false;
    public static boolean numberCheck = false;
    public static boolean specialCheck = false;
    public static boolean lengthCheck = false;

    static int passwordStrength = 0;

    static Scanner ms = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("---------Password Control---------");
        System.out.println("Please enter a password you'd like to test");

        while (true) {
            System.out.println(passwordCheck(ms.nextLine()));
            clear();
            System.out.println("---------Try another password---------");
        }
    }

    // Checks parameters for the password strength.
    public static String passwordCheck(String password) {
        if (Pattern.matches(lowerRegex, password)) {
            passwordStrength++;
            lowerCheck = true;
        }
        if (password.matches(upperRegex)) {
            passwordStrength++;
            upperCheck = true;
        }
        if (password.matches(numberRegex)) {
            passwordStrength++;
            numberCheck = true;
        }
        if (password.matches(specialRegex)) {
            passwordStrength++;
            specialCheck = true;
        }
        if (password.length() >= 8) {
            passwordStrength++;
            lengthCheck = true;
        }

        System.out.println(passwordHelper());
        if (passwordStrength == 5) {
            return "Your password is strong";
        } else if (passwordStrength >= 3) {
            return "Your password is moderate";
        } else if (passwordStrength >= 1) {
            return "Your password is weak";
        }
        return "Your password is invalid.";

    }

    // Helps the user with what can strengthen their password.
    public static StringBuilder passwordHelper() {
        StringBuilder returnString = new StringBuilder();
        if (!lowerCheck) {
            returnString.append("Your password will be stronger if you add a lowercase character\n");
        }
        if (!upperCheck) {
            returnString.append("Your password will be stronger if you add a uppercase character\n");
        }
        if (!numberCheck) {
            returnString.append("Your password will be stronger if you add a number\n");
        }
        if (!specialCheck) {
            returnString.append("Your password will be stronger if you add a special character\n");
        }
        if (!lengthCheck) {
            returnString.append("Your password will be stronger if you make it longer than 7 characters");
        }

        return returnString;
    }

    // Clear checks for a new password.
    public static void clear() {
        passwordStrength = 0;
        lowerCheck = false;
        upperCheck = false;
        specialCheck = false;
        lengthCheck = false;
        numberCheck = false;
    }

}
