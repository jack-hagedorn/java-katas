package hagedorn;

import java.util.Scanner;

public class Main {


    public static String convertTextToNumb(String phoneNumber) {

        return phoneNumber.toLowerCase()
                .replaceAll("[a-c]", "2")
                .replaceAll("[d-f]", "3")
                .replaceAll("[g-i]", "4")
                .replaceAll("[j-l]", "5")
                .replaceAll("[m-o]", "6")
                .replaceAll("[p-s]", "7")
                .replaceAll("[t-v]", "8")
                .replaceAll("[w-z]", "9");

    }

    public static void main(String[] args) {
        Scanner ms = new Scanner(System.in);

        System.out.println("Please enter phone number you want to convert.");

        String returnNumber = convertTextToNumb(ms.nextLine());

        System.out.println(returnNumber);
    }
}
