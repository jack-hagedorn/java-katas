package hagedorn;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Integer> myList = createIntegerArray(50);

        divideNumber(myList);

        System.out.println("TEST");

        ArrayList<String> newList = exchangeNumbers(myList);

        System.out.println(newList);

    }

    // Create an array of ints size based of parameter.
    public static ArrayList<Integer> createIntegerArray(int arraySize) {
        ArrayList<Integer> returnList = new ArrayList<>();
        int i = 1;
        while (i <= arraySize) {
            returnList.add(i);
            i++;
        }
        return returnList;
    }

    // Number control, will check if number is divisible by 3 or 5 or both.
    public static void divideNumber(ArrayList<Integer> myList) {
        for (int i : myList) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    // Exchanges the numbers in the array, dependent on same method as devideNumber.
    public static ArrayList<String> exchangeNumbers(ArrayList<Integer> myList){
        ArrayList<String> returnList = new ArrayList<>();
        for (int i: myList){
            if (i % 3 == 0 && i % 5 == 0) {
                returnList.add("FizzBuzz");
            } else if (i % 3 == 0) {
                returnList.add("Fizz");
            } else if (i % 5 == 0) {
                returnList.add("Buzz");
            } else {
                returnList.add(String.valueOf(i));
            }
        }
        return returnList;
    }
}
