package hagedorn.guitar;

public interface Playable {

    public String playSound();

}
