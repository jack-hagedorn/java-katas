package hagedorn.guitar;

public class ElectricGuitar extends Guitar {
    private String pickup;

    public ElectricGuitar(String brand, String model, int makeYear, String pickup) {
        super(brand, model, makeYear);
        this.pickup = pickup;
    }

    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    @Override
    public String playSound() {
        return "Electric music";
    }
}
