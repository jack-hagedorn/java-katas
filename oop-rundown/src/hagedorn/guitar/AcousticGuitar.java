package hagedorn.guitar;

public class AcousticGuitar extends Guitar{

    public AcousticGuitar(String brand, String model, int makeYear) {
        super(brand, model, makeYear);
    }

    @Override
    public String playSound() {
        return "Soft tunes";
    }
}
