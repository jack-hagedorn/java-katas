package hagedorn.guitar;

public abstract class Guitar implements Playable{
    private String brand;
    private String model;
    private int makeYear;

    public Guitar(String brand, String model, int makeYear) {
        this.brand = brand;
        this.model = model;
        this.makeYear = makeYear;
    }

    public String getInfo() {
        return "I'm a guitar";
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getMakeYear() {
        return makeYear;
    }

    public void setMakeYear(int makeYear) {
        this.makeYear = makeYear;
    }
}
