package hagedorn;

import hagedorn.guitar.ElectricGuitar;
import hagedorn.guitar.Guitar;

public class Main {

    public static void main(String[] args) {
        ElectricGuitar myElectricGuitar = new ElectricGuitar("Fender","2020",2020, "EMG");
        System.out.println(myElectricGuitar.playSound());
    }
}
