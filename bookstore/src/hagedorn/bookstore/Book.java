package hagedorn.bookstore;


import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class Book {

    String titel;
    int bookId;
    int price;
    HashMap<Integer, Book> cart = new HashMap<>();
    HashMap<Book, Integer> bookCount = new HashMap<>();


    // Constructor.
    public Book(String titel, int bookId, int price) {
        this.titel = titel;
        this.bookId = bookId;
        this.price = price;
    }

    // Add book to hashmap.
    public void addBook(Book book) {
        cart.put(book.bookId, book);
    }

    // Get book occurrences in the cart.
    public void bookOccurrence(HashMap<Integer, Book> booksMap) {
        booksMap.forEach((key, value) -> {
            if (bookCount.containsKey(key)) {
                bookCount.put(value, bookCount.get(key) + 1);
            } else {
                bookCount.put(value, 1);
            }
        });
    }

    // Calculate the price of the books.
    public double price(HashMap<Book, Integer> booksMap) {
        AtomicInteger differentBooks = new AtomicInteger();
        AtomicInteger price = new AtomicInteger();
        booksMap.forEach((key, value) -> {
            differentBooks.addAndGet(1);
            price.addAndGet(value * 8);
        });

        if (differentBooks.get() == 1) {
            return price.get();
        } else if (differentBooks.get() == 2) {
            return price.get() * 0.95;  // Discount is 5%;
        } else if (differentBooks.get() == 3) {
            return price.get() * 0.9;   // Discount is 10%;
        } else if (differentBooks.get() >= 4) {
            return price.get() * 0.8;   // Discount is 20%;
        }
        return 0;
    }

    // Clears cart.
    public void clearCart() {
        cart.clear();
    }
}

