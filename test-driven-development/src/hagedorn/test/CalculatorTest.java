package hagedorn.test;

import hagedorn.calculator.Calculator;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    static Calculator calculator = new Calculator();

    @Test
    public void additionCalcTest(){
        int n1 = 1;
        int n2 = 1;
        int expected = 2;

        int actual = calculator.additionCalc(n1, n2);

        assert actual == expected;
    }

    @Test
    public void divisionCalcTest(){
        int n1 = 10;
        int n2 = 5;
        int expected = 2;

        int actual = calculator.divisionCalc(n1, n2);

        assert actual == expected;
    }

    @Test
    public void multiplyCalcTest(){
        int n1 = 5;
        int n2 = 2;
        int expected = 10;

        int actual = calculator.multiplyCalc(n1, n2);

        assert actual == expected;
    }

    @Test
    public void subtractionCalcTest(){
        int n1 = 5;
        int n2 = 4;
        int expected = 1;

        int actual = calculator.subtractCalc(n1, n2);

        assert actual == expected;

    }

}
