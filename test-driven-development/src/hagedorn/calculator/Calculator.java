package hagedorn.calculator;

public class Calculator {

    public int additionCalc(int n1, int n2) {
        return n1 + n2;
    }

    public int divisionCalc(int n1, int n2) {
        return n1 / n2;
    }

    public int multiplyCalc(int n1, int n2) {
        return n1*n2;
    }

    public int subtractCalc(int n1, int n2) {
        return n1 - n2;
    }
}
